

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class viewStudent
 */
@WebServlet("/viewStudent")
public class viewStudent extends HttpServlet {
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/library","root","root");
			String qr="select * from student";
			java.sql.Statement st=con.createStatement();
			ResultSet rs=st.executeQuery(qr);
			if(rs.next())
			{
				out.println("<table align=center border=1px>");
				out.println("<th>Name</th>");
				out.println("<th>Id</th>");
				out.println("<th>Email</th>");
				//out.println("<th>Password</th>");
				out.println("<th>Address</th>");
				out.println("<th>Contact</th>");
				do
				{
					String name=rs.getString("name");
					String id=rs.getString("id");
					String  email=rs.getString("email");
					//String password=rs.getString("password");
					String address=rs.getString("address");
					String contact=rs.getString("contact");
					out.println("<tr>");
					out.println("<td>");
					out.println(name);
					out.println("</td>");
					out.println("<td>");
					out.println(id);
					out.println("</td>");
					out.println("<td>");
					out.println(email);
					out.println("</td>");
					
					out.println("<td>");
					out.println(address);
					out.println("</td>");
					out.println("<td>");
					out.println(contact);
					out.println("</td>");
					out.println("<td>");
					out.println(delete);
					out.println("</td>");
					out.println("<td>");
					out.println(Update);
					out.println("</td>");
					
					out.println("</tr>");
				}while(rs.next());
				out.println("</table>");
			}
			else
			{
				out.println("no records found");
			}
			con.close();
		} catch (Exception e) {
			out.println(e);
		}

	}

}
