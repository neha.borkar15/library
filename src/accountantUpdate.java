

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class accountantUpdate
 */
@WebServlet("/accountantUpdate")
public class accountantUpdate extends HttpServlet {
	
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		String name=request.getParameter("name");
		String id=request.getParameter("id");
		String password=request.getParameter("password");
		String email=request.getParameter("email");
		String address=request.getParameter("address");
		String contact=request.getParameter("contact");
		
		try{
			Class.forName("com.mysql.jdbc.Driver");			
			java.sql.Connection connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/library","root","root");			
			String qr="update into accountant values(?,?,?,?,?,?)";
			PreparedStatement preparedStatement = connection.prepareStatement(qr);
			
			preparedStatement.setString(1,name);
			preparedStatement.setString(2,id);
			preparedStatement.setString(3,password);
			preparedStatement.setString(4,email);
			preparedStatement.setString(5,address);
			preparedStatement.setString(6,contact);
			
			int i = preparedStatement.executeUpdate();
			out.println(i+" added");
			connection.close();
			RequestDispatcher re=request.getRequestDispatcher("adminHome.html");
		       re.include(request,response);
		}
		catch(Exception e)
		{
			out.println(e);
		}
	}
	}

}
