

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class studentadd
 */
@WebServlet("/adminadd")
public class adminadd extends HttpServlet {
	
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		String name=request.getParameter("name");
		String id=request.getParameter("id");
		String password=request.getParameter("password");
		String email=request.getParameter("email");
		String address=request.getParameter("address");
		String contact=request.getParameter("contact");
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/library","root","root");
            String qr="insert into admin1 values(?,?,?,?,?,?)";
			PreparedStatement ps=connection.prepareStatement(qr);
			ps.setString(1,name);
			ps.setString(2,id);
			ps.setString(3,password);
			ps.setString(4,email);
			ps.setString(5,address);
			ps.setString(6,contact);
			int i = ps.executeUpdate();
			out.println(i+"added");
		  connection.close();
						
		}catch(Exception e){
	       out.println(e);
			
		}
	}

}
